package org.eclipse.kura.demo.heater;


	import java.util.Map;
	import java.util.concurrent.Executors;
	import java.util.concurrent.ScheduledExecutorService;


	import org.eclipse.kura.cloudconnection.listener.CloudConnectionListener;
	import org.eclipse.kura.cloudconnection.listener.CloudDeliveryListener;
	import org.eclipse.kura.cloudconnection.message.KuraMessage;
	import org.eclipse.kura.cloudconnection.subscriber.CloudSubscriber;
	import org.eclipse.kura.cloudconnection.subscriber.listener.CloudSubscriberListener;
	import org.eclipse.kura.configuration.ConfigurableComponent;
	import org.osgi.service.component.ComponentContext;
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	public class Subscriber implements ConfigurableComponent, CloudSubscriberListener, CloudConnectionListener, CloudDeliveryListener {

	    private static final Logger logger = LoggerFactory.getLogger(Publisher.class);

	    // Publishing Property Names

	    private final ScheduledExecutorService worker;

	    

	    private CloudSubscriber cloudSubscriber;

	    // ----------------------------------------------------------------
	    //
	    // Dependencies
	    //
	    // ----------------------------------------------------------------

	    public Subscriber() {
	        super();
	        this.worker = Executors.newSingleThreadScheduledExecutor();
	    }

	    
	    public void setCloudSubscriber(CloudSubscriber cloudSubscriber) {
	    	this.cloudSubscriber = cloudSubscriber;
	        this.cloudSubscriber.registerCloudSubscriberListener(Subscriber.this);
	        this.cloudSubscriber.registerCloudConnectionListener(Subscriber.this);
	        
	    }

	    public void unsetCloudSubscriber(CloudSubscriber cloudSubscriber) {
	    	this.cloudSubscriber.unregisterCloudSubscriberListener(Subscriber.this);
	        this.cloudSubscriber.unregisterCloudConnectionListener(Subscriber.this);
	        this.cloudSubscriber = null;
	    }

	    // ----------------------------------------------------------------
	    //
	    // Activation APIs
	    //
	    // ----------------------------------------------------------------

	    protected void activate(ComponentContext componentContext, Map<String, Object> properties) {
	        logger.info("Activating Subscriber...");

	    }

	    protected void deactivate(ComponentContext componentContext) {
	        logger.debug("Deactivating Subscriber...");

	        // shutting down the worker and cleaning up the properties
	        this.worker.shutdown();

	        logger.debug("Deactivating Subscriber... Done.");
	    }

	    public void updated(Map<String, Object> properties) {
	        logger.info("Updated Subscriber...");
	        logger.info("Updated Subscriber... Done.");
	    }

	    // ----------------------------------------------------------------
	    //
	    // Cloud Application Callback Methods
	    //
	    // ----------------------------------------------------------------

	    @Override
	    public void onConnectionLost() {
	        // TODO Auto-generated method stub

	    }

	    @Override
	    public void onConnectionEstablished() {
	        // TODO Auto-generated method stub

	    }

	    @Override
	    public void onMessageConfirmed(String messageId) {
	        // TODO Auto-generated method stub

	    }

	    @Override
	    public void onDisconnected() {
	        // TODO Auto-generated method stub

	    }

	    // ----------------------------------------------------------------
	    //
	    // Private Methods
	    //
	    // ----------------------------------------------------------------

	    	
		@Override
		public void onMessageArrived(KuraMessage arg0) {
			 logger.info("Recibio un mensaje!!!!!");
		}
	

}
